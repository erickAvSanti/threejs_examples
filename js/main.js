

(function() {
  const camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10 );
  camera.position.z = 1;

  const scene = new THREE.Scene();

  const geometry = new THREE.BoxGeometry( 1, 1, 1 );
  const material = new THREE.MeshNormalMaterial();

  const mesh = new THREE.Mesh( geometry, material );
  mesh.position.y = -0.5;
  mesh.position.z = -2;
  scene.add( mesh );

  const canvas = document.getElementById('threejs_render');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  const renderer = new THREE.WebGLRenderer( {canvas: canvas, antialias: true } );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.setAnimationLoop( animation );
  // renderer.setClearColor(new THREE.Color(100,0,0), 1);

  // animation

  function animation( time ) {
    renderer.render( scene, camera );
  }

  window.addEventListener('resize', function(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    renderer.setSize( window.innerWidth, window.innerHeight );
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
  }, false);




  const map = new THREE.TextureLoader().load( '/assets/img/floor1.jpg' );
  const sprite_material = new THREE.MeshBasicMaterial( { map: map, color: 0xffffff, side: THREE.DoubleSide } );

  const plane_geo = new THREE.PlaneGeometry( 10, 10 );
  const plane = new THREE.Mesh( plane_geo, sprite_material );
  plane.position.z = -1;
  plane.position.y = -1;
  plane.rotateX( Math.PI / 2);
  scene.add( plane );





  const touch_screen = document.getElementById('touch_screen');
  touch_screen.width = window.innerWidth;
  touch_screen.height = window.innerHeight;
  const touch_screen_ctx = touch_screen.getContext('2d');
  

  function draw_on_touch_screen(){
    touch_screen_ctx.beginPath();
    touch_screen_ctx.strokeStyle = 'white';
    touch_screen_ctx.arc(75, 75, 50, 0, 2 * Math.PI);
    touch_screen_ctx.stroke();
  }
  draw_on_touch_screen();
  window.addEventListener('resize', function(){
    touch_screen.width = window.innerWidth;
    touch_screen.height = window.innerHeight;
    draw_on_touch_screen();
  }, false);



})();